export const DOMAIN = "http://devtest2-001-site1.htempurl.com/";
export const API = DOMAIN + "api/";

export function fileExists(src, callback) {
    var http = new XMLHttpRequest();

    http.onreadystatechange = (e) => {
        callback(http.status !== 404);
    }

    http.open('HEAD', src);
    http.send();
}