const select = data => {
    return data ? Object.keys(data).map(key => data[key]) : []
}

export default select;