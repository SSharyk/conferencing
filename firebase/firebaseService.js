import firebaseConfig from './config'
import * as firebase from 'firebase'

if (typeof global.self === "undefined") {
    global.self = global;
}

let instance = null

class FirebaseService {
    constructor() {
        if (!instance) {
            this.app = firebase.initializeApp(firebaseConfig);
            instance = this;
        }
        return instance
    }
}

const firebaseService = new FirebaseService().app
export default firebaseService;