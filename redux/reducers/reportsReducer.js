import * as types from '../actions/actionTypes'

const initialState = {
    refreshing: false,
    createReportError: null,
    selectedReport: null,
    sending: false,
}

const reports = (state = initialState, action) => {
    switch (action.type) {
        case types.REPORT_CREATE:
            return { ...state, refreshing: true }
        case types.REPORT_CREATE_SUCCESS:
            return { ...state, createEventError: null, refreshing: false, selectedReport: action.report };
        case types.REPORT_CREATE_ERROR:
            return { ...state, events: state.events, createEventError: action.error, refreshing: false }

        case types.REPORT_SELECTED:
            return { ...state, selectedReport: action.report };

        case types.REPORT_MESSAGE_SENDING_START:
            return { ...state, sending: true };
        case types.REPORT_MESSAGE_SENDING_FINISHED:
            return { ...state, sending: false };

        default:
            return state
    }
}

export default reports