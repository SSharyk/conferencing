import * as types from '../actions/actionTypes'

const initialState = {
    refreshing: false,
    events: [],
    loadEventsError: null,
    createEventError: null,
    currentEvent: null,
}

const events = (state = initialState, action) => {
    switch (action.type) {
        case types.EVENTS_LIST_LOAD:
            return { ...state, refreshing: true }
        case types.EVENTS_LIST_LOAD_SUCCESS:
            return { ...state, events: action.events, loadEventsError: null, refreshing: false, }
        case types.EVENTS_LIST_LOAD_ERROR:
            return { ...state, events: [], loadEventsError: action.error, refreshing: false }

        case types.EVENT_SELECTED:
            return { ...state, currentEvent: action.event };
        case types.EVENT_CREATE:
            return { ...state, refreshing: true }
        case types.EVENT_CREATE_SUCCESS:
        case types.EVENT_UPDATE_SUCCESS:
            return { ...state, createEventError: null, refreshing: false };
        case types.EVENT_CREATE_ERROR:
        case types.EVENT_UPDATE_ERROR:
        case types.EVENT_HIDE_ERROR:
            return { ...state, events: state.events, createEventError: action.error, refreshing: false }
        case types.EVENT_HIDE_SUCCESS:
            return { ...state, currentEvent: undefined }

        default:
            return state
    }
}

export default events