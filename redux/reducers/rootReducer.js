import { combineReducers } from 'redux'

import session from './sessionReducer';
import events from './eventsReducer';
import reports from './reportsReducer';

const rootReducer = combineReducers({
    session,
    events,
    reports,
});
export default rootReducer;