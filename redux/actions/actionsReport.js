import * as creators from './actionCreators';
import firebaseService from "../../firebase/firebaseService";
import select from '../../firebase/dataSelector';

const FIREBASE_REF_REPORTS = firebaseService.database().ref('Reports');
const FIREBASE_REF_EVENTS = firebaseService.database().ref('Events');
const FIREBASE_LIMIT = 20;

export const createNewReport = (report, onSuccess, onError) => {
    return (dispatch) => {
        dispatch(creators.createReportLoading());

        FIREBASE_REF_REPORTS.push().set(report, (error) => {
            if (error) {
                dispatch(creators.createReportError(error.message));
                onError(error);
            } else {
                dispatch(creators.createReportSuccess(report));

                FIREBASE_REF_EVENTS.orderByChild("id").equalTo(report.eventId).once("value", (snapshot) => {
                    snapshot.forEach(child => {
                        let sections = child.val().sections;
                        let index = -1;
                        for (let i = 0; i < sections.length; i++) {
                            if (sections[i].name == report.sectionTitle) {
                                index = i;
                                break;
                            }
                        }
                        let specificSection = sections[index];
                        if (specificSection.reports) {
                            specificSection.reports.push(report);
                        } else {
                            specificSection.reports = [report];
                        }
                        child.ref.update({
                            sections: sections,
                        });
                        dispatch(creators.updateEventSuccess(child.val()));
                    });
                });

                onSuccess(report);
            }
        });
    }
}

export const reportSelected = (report) => {
    return (dispatch) => {
        FIREBASE_REF_REPORTS.orderByChild("id").equalTo(report.id).on("value", (snapshot) => {
            snapshot.forEach(child => {
                dispatch(creators.reportSelected(child));
            });
        });
    }
}

export const sendMessage = (messageModel, onSuccess, onFailure) => {
    return (dispatch) => {
        dispatch(creators.sendingStarted());
        FIREBASE_REF_REPORTS.orderByChild("id").equalTo(messageModel.reportId).once("value", (snapshot) => {
            snapshot.forEach(child => {
                let oldMessages = select(child.val().messages);
                oldMessages.push(messageModel);
                child.ref.update({
                    messages: oldMessages,
                });
            });
            dispatch(creators.sendingFinished());
            if (onSuccess) {
                onSuccess();
            }
        });
    }
}