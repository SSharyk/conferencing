import * as types from './actionTypes';

// region SESSION

export const sessionRestoring = () => ({
    type: types.SESSION_RESTORING
})

export const sessionLoading = () => ({
    type: types.SESSION_LOADING
})

export const sessionSuccess = user => ({
    type: types.SESSION_SUCCESS,
    user
})

export const sessionError = error => ({
    type: types.SESSION_ERROR,
    error
})

export const sessionLogout = () => ({
    type: types.SESSION_LOGOUT
})

// endregion

// region EVENTS

export const eventsListLoading = () => ({
    type: types.EVENTS_LIST_LOAD,
});

export const loadEventsSuccess = events => ({
    type: types.EVENTS_LIST_LOAD_SUCCESS,
    events,
});

export const loadEventsError = error => ({
    type: types.EVENTS_LIST_LOAD_ERROR,
    error,
});

export const eventSelected = event => ({
    type: types.EVENT_SELECTED,
    event,
});

export const createEventLoading = () => ({
    type: types.EVENT_CREATE,
});

export const createEventSuccess = event => ({
    type: types.EVENT_CREATE_SUCCESS,
    event,
});

export const createEventError = error => ({
    type: types.EVENT_CREATE_ERROR,
    error,
});

export const updateEventSuccess = event => ({
    type: types.EVENT_UPDATE_SUCCESS,
    event,
});

export const updateEventError = error => ({
    type: types.EVENT_UPDATE_ERROR,
    error,
});

export const hideEventSuccess = event => ({
    type: types.EVENT_HIDE_SUCCESS,
    event,
});

export const hideEventError = error => ({
    type: types.EVENT_HIDE_ERROR,
    error,
});

// endregion

// region REPORTS

export const createReportLoading = () => ({
    type: types.REPORT_CREATE,
});

export const createReportSuccess = report => ({
    type: types.REPORT_CREATE_SUCCESS,
    report,
});

export const createReportError = error => ({
    type: types.REPORT_CREATE_ERROR,
    error,
});

export const reportSelected = report => ({
    type: types.REPORT_SELECTED,
    report,
});

export const sendingStarted = () => ({
    type: types.REPORT_MESSAGE_SENDING_START,
});

export const sendingFinished = () => ({
    type: types.REPORT_MESSAGE_SENDING_FINISHED,
});

// endregion