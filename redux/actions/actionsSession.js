import * as creators from './actionCreators';
import firebaseService from '../../firebase/firebaseService'
import AsyncStorageHelper from '../../utilities/AsyncStorageHelper';

import UserModel from '../../models/user';
import select from '../../firebase/dataSelector';

const FIREBASE_REF_USERS = firebaseService.database().ref('Users')

export const restoreSession = () => {
    return (dispatch) => {
        dispatch(creators.sessionRestoring())

        let unsubscribe = firebaseService.auth()
            .onAuthStateChanged(user => {
                if (user) {
                    AsyncStorageHelper.set(AsyncStorageHelper.KEY_USER, user);
                    dispatch(creators.sessionSuccess(user))
                    unsubscribe()
                } else {
                    dispatch(creators.sessionLogout())
                    unsubscribe()
                }
            })
    }
}

export const loginUser = (email, password) => {
    return (dispatch) => {
        dispatch(creators.sessionLoading())

        if (!email || email.length < 3) {
            let error = { code: "auth/invalid-email" };
            let localeMessage = getHumanReadableErrorMessage(error);
            dispatch(creators.sessionError(localeMessage))
            return;
        }
        if (!password || password.length < 3) {
            let error = { code: "auth/wrong-password" };
            let localeMessage = getHumanReadableErrorMessage(error);
            dispatch(creators.sessionError(localeMessage))
            return;
        }

        firebaseService.auth()
            .signInWithEmailAndPassword(email, password)
            .catch(error => {
                if (error.code) {
                    console.debug("ERROR: " + JSON.stringify(error));
                    let localeMessage = getHumanReadableErrorMessage(error);
                    dispatch(creators.sessionError(localeMessage));
                } else {
                    console.debug("Error while sign in: " + error);

                    FIREBASE_REF_USERS.on("value", snap => {
                        let allUsers = select(snap.val());
                        let logged = allUsers.filter((item) => item.email == email);
                        if (logged && logged.length > 0) {
                            logged = logged[0];
                            console.debug("LOGGED: " + JSON.stringify(logged));
                            AsyncStorageHelper.set(AsyncStorageHelper.KEY_USER, logged);
                        }
                    });
                }
            })

        let unsubscribe = firebaseService.auth()
            .onAuthStateChanged(user => {
                if (user) {
                    //AsyncStorageHelper.set(AsyncStorageHelper.KEY_USER, user);
                    dispatch(creators.sessionSuccess(user))
                    unsubscribe()
                }
            })
    }
}

export const signupUser = (email, password, username, role) => {
    return (dispatch) => {
        dispatch(creators.sessionLoading())

        if (!email || email.length < 3) {
            let error = { code: "auth/invalid-email" };
            let localeMessage = getHumanReadableErrorMessage(error);
            dispatch(creators.sessionError(localeMessage))
            return;
        }
        if (!password || password.length < 3) {
            let error = { code: "auth/wrong-password" };
            let localeMessage = getHumanReadableErrorMessage(error);
            dispatch(creators.sessionError(localeMessage))
            return;
        }
        if (!username || username.length < 3) {
            let error = { code: "validation/invalid-username" };
            let localeMessage = getHumanReadableErrorMessage(error);
            dispatch(creators.sessionError(localeMessage))
            return;
        }

        firebaseService.auth()
            .createUserWithEmailAndPassword(email, password)
            .catch(error => {
                if (error.code) {
                    let localeMessage = getHumanReadableErrorMessage(error);
                    dispatch(creators.sessionError(localeMessage));
                } else {
                    console.debug("Error while sign up: " + error);
                    let newUser = new UserModel(email, username, role);
                    FIREBASE_REF_USERS.push().set(newUser, (error) => {
                        if (error) {
                            let localeMessage = getHumanReadableErrorMessage(error);
                            dispatch(creators.sessionError(localeMessage));
                        } else {
                            dispatch(creators.sessionSuccess(newUser));
                            AsyncStorageHelper.set(AsyncStorageHelper.KEY_USER, newUser);
                        }
                    })
                }
            })

        let unsubscribe = firebaseService.auth()
            .onAuthStateChanged(user => {
                if (user) {
                    dispatch(creators.sessionSuccess(user))
                    unsubscribe()
                }
            })
    }
}

export const logoutUser = () => {
    return (dispatch) => {
        dispatch(creators.sessionLoading())

        firebaseService.auth()
            .signOut()
            .then(() => {
                AsyncStorageHelper.delete(AsyncStorageHelper.KEY_USER);
                dispatch(creators.sessionLogout())
            })
            .catch(error => {
                let localeMessage = getHumanReadableErrorMessage(error);
                dispatch(creators.sessionError(localeMessage));
            })
    }
}

const getHumanReadableErrorMessage = (error) => {
    if (!error) {
        return "Во время авторизации произошла ошибка. Пожалуйста, повторите попытку позднее"
    };

    switch (error.code) {
        case "auth/invalid-email": return "Неверный формат электронной почты";
        case "auth/weak-password": return "Пароль должен содержать не менее 6 символов";
        case "auth/email-already-in-use": return "Пользователь с таким адресов электронной почты уже зарегистрирован в системе";
        case "auth/email-already-exists": return "Пользователь с таким адресов электронной почты уже зарегистрирован в системе";
        case "auth/user-not-found": return "Пользователь с таким адресом электронной почты не обнаружен";
        case "auth/wrong-password": return "Неверный пароль";
        case "validation/invalid-username": return "Некорректное имя пользователя";
        default: return error.code;
    }
}