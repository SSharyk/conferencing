import * as creators from './actionCreators';
import firebaseService from "../../firebase/firebaseService";
import select from '../../firebase/dataSelector';
import { EVENT_STATUSES, EventStatusTitles } from '../../models/event';

const FIREBASE_REF_EVENTS = firebaseService.database().ref('Events');
const FIREBASE_LIMIT = 20;

export const loadEventsList = () => {
    return (dispatch) => {
        console.debug('1. Extract all events');
        dispatch(creators.eventsListLoading());
        FIREBASE_REF_EVENTS.limitToLast(FIREBASE_LIMIT).on('value', (snapshot) => {
            console.debug('1.1. Events: ' + JSON.stringify(snapshot));
            dispatch(creators.loadEventsSuccess(snapshot.val()))
        }, (errorObject) => {
            console.debug('1.2. Events: ' + JSON.stringify(errorObject));
            dispatch(cerators.loadEventsError(errorObject.message))
        })
    };
}

export const createNewEvent = (event, onSuccess, onError) => {
    return (dispatch) => {
        //dispatch(creators.createEventLoading());

        FIREBASE_REF_EVENTS.push().set(event, (error) => {
            if (error) {
                dispatch(creators.createEventError(error.message));
                onError(error);
            } else {
                dispatch(creators.createEventSuccess(event));
                onSuccess(event);
            }
        })
    }
}

export const update = (event, onSuccess, onError) => {
    return (dispatch) => {
        FIREBASE_REF_EVENTS.orderByChild("id").equalTo(event.id).once("value", (snapshot) => {
            snapshot.forEach(child => {
                child.ref.update(event);
                onSuccess();
            });
        });
    }
}

export const eventSelected = (event) => {
    return (dispatch) => {
        FIREBASE_REF_EVENTS.orderByChild("id").equalTo(event.id).on("value", (snapshot) => {
            snapshot.forEach(child => {
                dispatch(creators.eventSelected(child));
            });
        });
    }
}

export const subscribe = (event, userEmail) => {
    return (dispatch) => {
        let newSubscribersList = select(event.subscribers);
        newSubscribersList.push(userEmail);
        FIREBASE_REF_EVENTS.orderByChild("id").equalTo(event.id).once("value", (snapshot) => {
            snapshot.forEach(child => {
                child.ref.update({
                    subscribers: newSubscribersList,
                });
            });
        });
    }
}

export const unsubscribe = (event, userEmail) => {
    return (dispatch) => {
        let newSubscribersList = select(event.subscribers);
        let index = newSubscribersList.indexOf(userEmail);
        newSubscribersList.splice(index, 1);
        FIREBASE_REF_EVENTS.orderByChild("id").equalTo(event.id).once("value", (snapshot) => {
            snapshot.forEach(child => {
                child.ref.update({
                    subscribers: newSubscribersList,
                });
            });
            dispatch(creators.updateEventSuccess(event));
        });
    }
}

export const createSection = (event, sectionName) => {
    return (dispatch) => {
        let sections = select(event.sections);
        let addingSection = {
            name: sectionName,
            reports: [],
        };
        sections.push(addingSection);
        FIREBASE_REF_EVENTS.orderByChild("id").equalTo(event.id).on("value", (snapshot) => {
            snapshot.forEach(child => {
                child.ref.update({
                    sections: sections,
                });
                dispatch(creators.updateEventSuccess(child.val()));
            });
        });
    }
}

export const cancelEvent = (eventId, onSuccess, onError) => {
    return changeStatus(eventId, EVENT_STATUSES.Скрыто, onSuccess, onError);

    /*
    return (dispatch) => {
            let status = EVENT_STATUSES.Скрыто;
            FIREBASE_REF_EVENTS.orderByChild("id").equalTo(eventId).on("value", (snapshot) => {
                snapshot.forEach(child => {
                    child.ref.update({
                        eventStatusId: status,
                        eventStatus: {
                            status: EventStatusTitles[status - 1],
                            eventStatusId: status,
                        }
                    });
                    dispatch(creators.hideEventSuccess(child.val()));
                });
                onSuccess();
            });
    }
    */
}

export const deleteEvent = (eventId, onSuccess, onError) => {
    return changeStatus(eventId, EVENT_STATUSES.Удалено, onSuccess, onError);
    /*
    return (dispatch) => {
            let status = EVENT_STATUSES.Скрыто;
            FIREBASE_REF_EVENTS.orderByChild("id").equalTo(eventId).on("value", (snapshot) => {
                snapshot.forEach(child => {
                    child.ref.remove();
                    dispatch(creators.hideEventSuccess(child.val()));
                });
                onSuccess();
            });
    }
    */
}

export const restoreEvent = (eventId, onSuccess, onError) => {
    return changeStatus(eventId, EVENT_STATUSES.Запланировано, onSuccess, onError);
}

const changeStatus = (eventId, status, onSuccess, onError) => {
    return (dispatch) => {
        FIREBASE_REF_EVENTS.orderByChild("id").equalTo(eventId).on("value", (snapshot) => {
            snapshot.forEach(child => {
                child.ref.update({
                    eventStatusId: status,
                    eventStatus: {
                        status: EventStatusTitles[status - 1],
                        eventStatusId: status,
                    }
                });
                dispatch(creators.hideEventSuccess(child.val()));
            });
            onSuccess();
        });
    }
}