import { TabNavigator } from 'react-navigation';

import { ReportDetailsScreen } from '../screens/ReportDetailsScreen';
import { ReportChatScreen } from '../screens/ReportChatScreen';

export const reportNavigationRoutes = {
    Details: ReportDetailsScreen,
    Chat: ReportChatScreen,
}

export const reportTabNavigationOptions = {
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state;
            return (<Text> {routeName} </Text>);
        },
    }),
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
    },
}

export const ReportTabNavigator = TabNavigator(reportNavigationRoutes, reportTabNavigationOptions);