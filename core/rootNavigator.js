import { TabNavigator } from 'react-navigation';

import ProfileConnect, { ProfileScreen } from '../screens/ProfileScreen';
import StatisticsConnect, { StatisticsScreen } from '../screens/StatisticsScreen';
import EventStackNavigator from './eventNavigator';

export const mainPageNavigationRoutes = {
    Расписание: EventStackNavigator,
    Профиль: ProfileConnect,
    Мои_события: StatisticsConnect,
}

export const mainPageTabNavigationOptions = {
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state;
            return (<Text> {routeName} </Text>);
        },
    }),
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: 'gray',
    },
}

const MainPageTabNavigator = TabNavigator(mainPageNavigationRoutes, mainPageTabNavigationOptions);
export default MainPageTabNavigator;