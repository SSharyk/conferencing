import { StackNavigator } from 'react-navigation'

import ScheduleConnect from '../screens/ScheduleScreen';
import NewEventConnect from '../screens/NewEventScreen';
import EventDetailConnect from '../screens/EventDetailsScreen';
import ReportDetailsConnect from '../screens/ReportDetailsScreen';
import ReportChatConnect from '../screens/ReportChatScreen';
import NewReportConnect from '../screens/NewReportScreen';

const eventStackNavigatorRoutes = {
    Event: {
        screen: ScheduleConnect,
    },
    NewEvent: {
        screen: NewEventConnect,
    },
    EventDetails: {
        screen: EventDetailConnect,
    },
    NewReport: {
        screen: NewReportConnect,
    },
    Report: {
        screen: ReportDetailsConnect,
    },
    Chat: {
        screen: ReportChatConnect,
    },
}

const eventStackNavigationConfig = {
    initialRouteName: "Event",

    mode: "card",
    headerMode: "screen"
}

const EventStackNavigator = StackNavigator(eventStackNavigatorRoutes, eventStackNavigationConfig);
export default EventStackNavigator;