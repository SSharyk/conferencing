import React, { Component } from 'react';
import { ActivityIndicator, Text, View, FlatList, TouchableHighlight, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Card } from 'react-native-elements';
import { connect } from 'react-redux';

import { EventRow } from '../components/EventRow';
import select from '../firebase/dataSelector';
import { loadEventsList, cancelEvent, deleteEvent, eventSelected } from '../redux/actions/actionsEvents';
import { restoreSession } from '../redux/actions/actionsSession';
import AsyncStorageHelper from '../utilities/AsyncStorageHelper';
import { EVENT_STATUSES } from '../models/event';

export class StatisticsScreen extends Component {

    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            user: {},
        }
        this.buttons = {};
    }

    async componentWillMount() {
        let user = await AsyncStorageHelper.get(AsyncStorageHelper.KEY_USER);

        if (user) {
            this.setState({
                user: user,
            });
        }
        let self = this;
        EventsRegistrator.addEventListener("sessionChanged", function (user) {
            self.setState({ user: user });
        });
    }

    componentDidMount() {
        this.props.restore();
    }

    async componentWillReceiveProps(nextProps, nextCtx) {
        let user = await AsyncStorageHelper.get(AsyncStorageHelper.KEY_USER);

        if (user) {
            this.setState({
                user: user,
            });
        }
    }

    render() {
        if (!this.state.user || !this.state.user.email) {
            return (
                <View style={[styles.centeredContainer, styles.loadingContainer]}>
                    <ActivityIndicator style={styles.loadingIndicator} />
                    <Card>
                        <View style={styles.centeredContainer}>
                            <Text style={styles.centeredLabel}> {"Войдите в систему, чтобы ознакомиться со своей статистикой"} </Text>
                        </View>
                    </Card>
                </View>
            );
        }

        let data = select(this.props.events);
        if (this.props.refreshing || !data) {
            return (
                <View style={[styles.centeredContainer, styles.loadingContainer]}>
                    <ActivityIndicator style={styles.loadingIndicator} />
                </View>
            );
        }

        const orgEvents = data.filter(this._filterOrgEvents);
        const subscrEvents = data.filter(this._filterSubscribedEvents);

        return (
            <View style={styles.eventsList}>
                <Card title="Организатор">
                    {
                        orgEvents.length > 0 &&
                        <FlatList
                            data={orgEvents}
                            renderItem={this._singleEventRender}
                            keyExtractor={this._singleEventKeyExtractor}
                            ItemSeparatorComponent={this._singleItemRenderSeparator} />
                    }
                    {
                        orgEvents.length == 0 &&
                        <Text style={styles.centeredLabel}> {"Вы не подписаны ни на одно событие"} </Text>
                    }
                </Card>

                <Card title="Подписка">
                    {
                        subscrEvents.length > 0 &&
                        <FlatList
                            data={subscrEvents}
                            renderItem={this._singleEventRender}
                            keyExtractor={this._singleEventKeyExtractor}
                            ItemSeparatorComponent={this._singleItemRenderSeparator} />
                    }
                    {
                        subscrEvents.length == 0 &&
                        <Text style={styles.centeredLabel}> {"Вы не подписаны ни на одно событие"} </Text>
                    }
                </Card>
            </View>
        );
    }

    _filterOrgEvents = (ev) => {
        if (ev.eventStatusId == EVENT_STATUSES.Удалено) return false;
        return ev.organizer.email == this.state.user.email;
    }
    _filterSubscribedEvents = (ev) => {
        if (ev.eventStatusId == EVENT_STATUSES.Удалено ||
            ev.eventStatusId == EVENT_STATUSES.Скрыто) return false;
        return select(ev.subscribers).indexOf(this.state.user.email) != -1
            && ev.organizer.email != this.state.user.email;
    }

    _singleEventRender = ({ item }) => {
        if (item.eventStatusId == EVENT_STATUSES.Заверешено) {
            return null;
        }

        return (
            <EventRow
                event={item}
                onPress={() => this._onSingleEventPress(item)} />
        );
    }

    _singleItemRenderSeparator = () => (
        <View style={styles.separator} />
    );
    _singleEventKeyExtractor = (item, index) => "" + item.id;

    _onSingleEventPress = (item) => {
        this.props.eventSelected(item);
        this.props.navigation.navigate('EventDetails', {
            event: item,
            id: item.id,
            title: item.name,
            user: this.state.user,
        });
    }
}

const mapStateToProps = state => ({
    user: state.session.user,
    refreshing: state.events.refreshing,
    events: state.events.events,
});
const mapDispatchToProps = {
    restore: restoreSession,
    load: loadEventsList,
    eventSelected: eventSelected,
};

const StatisticsConnect = connect(mapStateToProps, mapDispatchToProps)(StatisticsScreen);
export default StatisticsConnect;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    centeredContainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    centeredLabel: {
        fontSize: 32,
    },

    swipeButton: {
        flexDirection: "row",
    },
    swipeLabel: {
        fontSize: 22,
    }
});