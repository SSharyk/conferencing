import React, { Component } from 'react';
import { Text, Button, View, StyleSheet } from 'react-native'
import { PropTypes } from 'prop-types'
import { Card } from 'react-native-elements';
import { Gravatar, GravatarApi } from 'react-native-gravatar';

import { connect } from 'react-redux'

import { loginUser, signupUser, logoutUser, restoreSession } from '../redux/actions/actionsSession';
import { LoginForm } from '../components/LoginForm';
import AsyncStorageHelper from '../utilities/AsyncStorageHelper';
import EventsRegistrator from '../utilities/EventsRegistrator';

const ProfileComponent = props => {
    if (!props.user) {
        return (
            <LoginForm
                login={props.login}
                signup={props.signup}
                loading={props.loading}
                error={props.error} />
        );
    } else {
        let gravatarOptions = {
            email: props.user.email,
            parameters: { "size": GRAVATAR_RADIUS, "d": "wavatar" },
        };
        return (
            <View style={styles.container}>
                <Card
                    title="Личные данные">

                    <View styles={styles.personalInfoContainer}>
                        <View style={styles.infoContainer}>
                            <Gravatar
                                options={gravatarOptions}
                                style={styles.roundedProfileImage} />
                        </View>

                        <View style={styles.infoContainer}>
                            <Text style={[styles.info, styles.infoLabel]}> {"Email"} </Text>
                            <Text style={[styles.info, styles.infoValue]}> {props.user.email} </Text>
                        </View>
                        {
                            props.user.username &&
                            <View style={styles.infoContainer}>
                                <Text style={[styles.info, styles.infoLabel]}> {"Логин"} </Text>
                                <Text style={[styles.info, styles.infoValue]}> {props.user.username} </Text>
                            </View>
                        }
                    </View>
                </Card>

                <Card
                    title="Аккаунт">
                    <Button
                        style={styles.button}
                        title="Выход"
                        onPress={props.logout} />
                </Card>
            </View>
        );
    }
}


export class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentWillMount() {
        let user = await AsyncStorageHelper.get(AsyncStorageHelper.KEY_USER);
        if (user) {
            this.setState({
                user: user,
            });
        }
    }

    async componentWillReceiveProps(nextProps, nextCtx) {
        console.debug("PROFILE WILL RECEIVE")
        let storedUser = await AsyncStorageHelper.get(AsyncStorageHelper.KEY_USER);
        if (storedUser) {
            if (storedUser.email) {
                this.setState({ user: storedUser });
            } else {
                this.setState({ user: undefined });
            }
        } else {
            this.setState({ user: undefined });
        }
        EventsRegistrator.fire('sessionChanged', this.state.user);
    }

    componentDidMount() {
        this.props.restore();
    }

    render() {
        return (
            <ProfileComponent
                user={this.state.user || this.props.user}
                loading={this.props.loading}
                error={this.props.error}
                restore={this.props.restore}
                login={this.props.login}
                signup={this.props.signup}
                logout={this.props.logout}
            />
        );
    }
}

const mapStateToProps = state => ({
    user: state.session.user,
    loading: state.session.loading,
    error: state.session.error,
})

const mapDispatchToProps = {
    login: loginUser,
    signup: signupUser,
    logout: logoutUser,
    restore: restoreSession,
}

const GRAVATAR_RADIUS = 300;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'space-between',
    },
    personalInfoContainer: {
        alignItems: 'stretch',
        justifyContent: "center",
    },
    infoContainer: {
        flexDirection: "row",
    },
    info: {
        fontSize: 20,
        flex: 1,
    },
    infoValue: {
        fontWeight: "600",
    },
    infoLabel: {
        fontStyle: "italic",
    },
    roundedProfileImage: {
        flex: 1,
        width: GRAVATAR_RADIUS,
        height: GRAVATAR_RADIUS,
        marginVertical: 20,
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: GRAVATAR_RADIUS / 2,
    },
});

const ProfileConnect = connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
export default ProfileConnect;