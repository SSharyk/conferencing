import React, { Component } from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native'

import { connect } from 'react-redux';

import { ChatBottom } from '../components/ChatBottom';
import { ChatMessage } from '../components/ChatMessage';
import select from '../firebase/dataSelector';
import { sendMessage } from '../redux/actions/actionsReport';
import MessageModel from '../models/message';

export class ReportChatComponent extends Component {
    constructor(props) {
        super(props);

        const user = this.props.navigation.getParam('user', {});

        let report = this.props.report;
        if (report.val) {
            report = report.val();
        }

        const messages = report.messages;
        this.state = {
            user: user,
            messages: messages,
        }
    }

    componentWillReceiveProps(nextProps, nextCtx) {
        let report = nextProps.report;
        if (!report) {
            return;
        }
        if (nextProps.report.val) {
            report = report.val();
        }
        this.setState({ messages: report.messages });
    }

    render() {
        const messages = this.state.messages;

        return (
            <View style={styles.container}>
                <FlatList
                    ref={ref => this.flatList = ref}
                    onContentSizeChange={this._scroll}
                    onLayout={this._scroll}
                    style={[styles.messagesList]}
                    data={this.state.messages}
                    renderItem={this._singleMessageRender}
                    keyExtractor={this._singleMessageKeyExtractor} />

                {
                    this.state.user && this.state.user.email &&

                    <ChatBottom
                        style={styles.chatBottom}
                        user={this.state.user}
                        onSend={this._onSend.bind(this)} />
                }
            </View>
        );
    }

    _scroll = () => {
        if (this.state.messages && this.state.messages.length > 0) {
            this.flatList.scrollToEnd({ animated: true })
        }
    }

    _singleMessageRender = (message) => {
        return (
            <ChatMessage
                message={message}
                userEmail={this.state.user.email} />
        )
    }

    _singleMessageKeyExtractor = (mes, index) => "" + mes.reportFileId;

    _onSend = (text, onSuccess, onFailure) => {
        if (text && text.length > 0) {
            let message = MessageModel.createNew(this.state.user, this.props.reportId, text);
            this.props.send(message, onSuccess, onFailure);
        }
    }
}

export class ReportChatScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`
    });

    render() {
        const reportId = this.props.navigation.getParam('reportId', 0);
        return (
            <ReportChatComponent
                navigation={this.props.navigation}
                reportId={reportId}
                report={this.props.report}
                user={this.props.user}
                send={this.props.send} />
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.session.user,
    report: state.reports.selectedReport,
    isSending: state.reports.sending,
});
const mapDispatchToProps = {
    send: sendMessage,
};

const ReportChatConnect = connect(mapStateToProps, mapDispatchToProps)(ReportChatScreen);
export default ReportChatConnect;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-evenly",
    },
    centeredContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        flexDirection: 'column',
    },
    messagesList: {
        marginBottom: 20,
    },
    centeredLabel: {
        fontSize: 32,
    },
    chatBottom: {
        justifyContent: "flex-end",
        alignItems: "flex-end",
    },
});