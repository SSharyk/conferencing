import React, { Component } from 'react';
import { Alert, Text, Button, View, Image, FlatList, StyleSheet, TouchableHighlight } from 'react-native'
import { Gravatar, GravatarApi } from 'react-native-gravatar';
//import RNFetchBlob from 'react-native-fetch-blob'

import { connect } from 'react-redux';

import { Card } from 'react-native-elements';
import { DOMAIN } from "../services/http.service";
import { VisitingCard } from '../components/VisitingCard';

class ReportDetailsComponent extends Component {

    constructor(props) {
        super(props);
        self = this;

        const id = this.props.navigation.getParam('id', 'N/A');
        const item = this.props.navigation.getParam('report', {});
        const user = this.props.navigation.getParam('user', {});

        this.state = {
            id: item.id || id,
            fullReport: item || this.props.report,
            user: user,
        }
    }

    componentWillReceiveProps(nextProps, nextCtx) {
        let rep = nextProps.report;
        if (!rep) {
            // after hiding
            return;
        }
        if (rep.val) {
            rep = rep.val();
        }
        this.setState({ fullReport: rep });
    }

    render() {
        const eventId = this.state.fullReport.eventId;
        fetch(`${DOMAIN}Files/Logo/${eventId}.png`)
            .then(resp => this.setState({ hasLogo: +resp.status == 200 }));

        const report = this.state.fullReport;

        if (this.state.refreshing) {
            return (
                <View style={styles.centeredContainer}>
                    <Text style={styles.centeredLabel}> {`REFRESHING`} </Text>
                </View>
            );
        }

        if (this.state.error) {
            return (
                <Text style={styles.containerCentered}> {"NO DATA LOAD"}</Text>
            );
        };

        let email = (this.state.fullReport.organizer) ? this.state.fullReport.organizer.email : 'N/A';
        let login = (this.state.fullReport.organizer) ? this.state.fullReport.organizer.username : 'N/A';

        return (
            <View style={styles.container}>
                <View>
                    <Card title="Сводка">
                        <View style={styles.headerContainer}>
                            <View style={styles.logosContainer}>
                                <Image source={{ uri: `${DOMAIN}Files/Logo/${eventId}.png` }}
                                    resizeMode="cover"
                                    style={[styles.image, styles.logo, { flex: 1 }]} />
                            </View>

                            <View>
                                <Text style={styles.reportDescription}>{this.state.fullReport.description}</Text>
                                {
                                    this.state.error &&
                                    <Text style={styles.reportDescription}>{this.state.error}</Text>
                                }
                            </View>
                        </View>
                    </Card>

                    <VisitingCard 
                        title={"Автор"}
                        email={email}
                        login={login} />

                    {
                        this.state.fullReport.reportFiles && this.state.fullReport.reportFiles.length > 0 &&
                        <Card title="Прикрепленные файлы">
                            <FlatList
                                data={this.state.fullReport.reportFiles || []}
                                renderItem={this._singleFileRender}
                                keyExtractor={this._singleFileKeyExtractor} />
                        </Card>
                    }
                </View>

                <Card>
                    <Button
                        title={"Обсудить"}
                        onPress={this._moveToChat} />
                </Card>
            </View>
        );
    }

    _onReportLoaded = (rep) => {
        this.setState({ fullReport: rep, refreshing: false });
    }
    _onReportFailed = (err) => {
        this.setState({ error: err || "Rejected", refreshing: false });
    }

    _singleFileRender = ({ item }) => (
        <View style={styles.fileContainer}>
            <TouchableHighlight
                onPress={() => this._onFilePress(item)}>
                <Text style={styles.fileName}> {item.fileName} </Text>
            </TouchableHighlight>
        </View>
    )

    _singleFileKeyExtractor = (item, index) => "" + item.reportFileId;

    _onFilePress(file) {
        /*
        RNFetchBlob.config({
            addAndroidDownloads: {
                title: file.fileName,
                useDownloadManager: true,
                mediaScannable: true,
                fileCache: true,
                notification: true,
                description: 'File downloaded by download manager.',
                path: `${dirs.DownloadDir}/${file.fileName}`,
            },
        })
        .fetch('GET', `${DOMAIN}Files/${file.reportId}/${file.fileId}`)
        .progress((received, total) => {
            console.log('progress', received / total)
        })
        .then(this._onFileDownloadSuccess)
        .catch(this._onFileDownloadFailure);
        */
    }

    _onFileDownloadSuccess = (res) => {
        Alert.alert(
            'Загрузка файла',
            'Файл загружн успешно',
            [
                { text: 'OK', onPress: () => { } },
            ],
            { cancelable: false }
        );

        //const android = RNFetchBlob.android;
        const path = res.path();
        const mime = '*/*';
        android.actionViewIntent(path, mime);
    }

    _onFileDownloadFailure = (err) => {
        Alert.alert(
            'Загрузка файла',
            'Во время загрузки файла произошла ошибка',
            [
                { text: 'OK', onPress: () => { } },
            ],
            { cancelable: false }
        )
    }

    _moveToChat = () => {
        this.props.navigation.navigate('Chat', {
            reportId: this.state.fullReport.id,
            title: this.state.fullReport.name,
            user: this.state.user,
            report: this.state.fullReport,
        });
    }
}

export class ReportDetailsScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`
    });

    render() {
        return (
            <ReportDetailsComponent
                report={this.props.report}
                navigation={this.props.navigation} />
        );
    }
}

const mapStateToProps = (state) => ({
    report: state.reports.selectedReport,
});
const mapDispatchToProps = {

};
const ReportDetailsConnect = connect(mapStateToProps, mapDispatchToProps)(ReportDetailsScreen);
export default ReportDetailsConnect;

const GRAVATAR_RADIUS = 100;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-between",
    },

    headerContainer: {
        flexDirection: 'row',
        margin: 15,
    },
    reportDescription: {
        fontSize: 18,
        fontWeight: "600",
        margin: 8,
    },
    reportAuthor: {
        width: "50%",
    },

    logosContainer: {
        flexDirection: "row"
    },
    image: {
        width: GRAVATAR_RADIUS,
        height: GRAVATAR_RADIUS,
    },
    logo: {
        justifyContent: "flex-start",
    },
    avatar: {
        justifyContent: "flex-end",
    },

    fileName: {
        fontStyle: "italic",
        fontSize: 18,
    },

    centeredContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        flexDirection: 'column',
    },
    centeredLabel: {
        fontSize: 32,
    },
});