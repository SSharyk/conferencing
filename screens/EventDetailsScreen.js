import React, { Component } from 'react';
import { Alert, Text, View, Button, Image, StyleSheet, SectionList, TouchableHighlight } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';

import { connect } from 'react-redux';

import { DOMAIN } from "../services/http.service";
import { formatDate } from "../utilities/formatter";
import SubscriptionButtonConnect from '../components/SubscriptionButton';
import { USER_ROLE_ORGANIZER, USER_ROLE_REPORTER, USER_ROLE_LISTENER } from '../models/user';
import Prompt from '../components/Prompt';
import select from '../firebase/dataSelector';
import { createSection, cancelEvent, deleteEvent, restoreEvent } from '../redux/actions/actionsEvents';
import { reportSelected } from '../redux/actions/actionsReport';
import { Card } from 'react-native-elements';
import { EVENT_STATUSES } from '../models/event';
import { VisitingCard } from '../components/VisitingCard';

var self;

class EventDetailsComponent extends Component {
    constructor(props) {
        super(props);
        self = this;

        //const { navigation } = this.props;
        const id = this.props.navigation.getParam('id', 'N/A');
        const item = this.props.navigation.getParam('event', {});
        const user = this.props.navigation.getParam('user', {});
        const userEmail = (user && user.email) ? user.email : undefined;

        this.state = {
            id: item.id || id,
            fullEvent: item,
            reportsInSections: this._convertToSectionList(item),
            selectedSectionId: 0,
            user: user,
            userEmail: userEmail,
            isSubscribed: true,
            createSectionDialogVisible: false,
            refresh: true,
        }
    }

    componentWillReceiveProps(nextProps, nextCtx) {
        let ev = nextProps.event;
        if (!ev) {
            // after hiding
            return;
        }
        if (ev.val) {
            ev = ev.val();
        }
        this.setState({ fullEvent: ev, reportsInSections: this._convertToSectionList(ev) });
    }

    render() {
        fetch(`${DOMAIN}Files/Logo/${this.state.id}.png`)
            .then(resp => this.setState({ hasLogo: +resp.status == 200 }));

        const event = this.state.fullEvent;
        const tags = (event.tags || "").split(',');
        let email = (event.organizer) ? event.organizer.email : 'N/A';
        let login = (event.organizer) ? event.organizer.username : 'N/A';

        return (
            <View style={styles.container}>
                <Card
                    title={"Сводка"}>
                    <View style={styles.headerContainer}>
                        {
                            this.state.hasLogo &&
                            <Image source={{ uri: `${DOMAIN}Files/Logo/${this.state.id}.png` }}
                                resizeMode="contain"
                                style={{ flex: 1 }} />
                        }
                        <View style={{ flex: 3 }}>
                            <Text style={styles.eventDescription}>{event.description}</Text>
                            <View style={styles.eventTagsContainer}>
                                {
                                    tags.map(t => <Text style={styles.tag} key={t}> {t} </Text>)
                                }
                            </View>
                            <View style={styles.eventStatusContainer}>
                                <Text style={styles.eventStatus}>{event.eventStatus.status}</Text>
                                <Text style={styles.eventDate}>{formatDate(event.date, "DD.MM.YYYY")}</Text>
                            </View>
                        </View>
                    </View>
                </Card>


                <VisitingCard
                    title={"Организатор"}
                    email={email}
                    login={login} />

                <SectionList
                    style={styles.list}
                    extraData={this.state.refresh}
                    sections={this.state.reportsInSections}
                    renderSectionHeader={this._renderSection}
                    renderItem={this._renderReport} />

                {
                    this.state.reportsInSections.length == 0 &&
                    <Card>
                        <View style={styles.centeredContainer}>
                            <Text style={styles.centeredLabel}> {"Нет ни одного доклада"} </Text>
                        </View>
                    </Card>
                }

                <View>
                    <Card>
                        <SubscriptionButtonConnect
                            event={this.state.fullEvent}
                            userEmail={this.state.userEmail} />
                    </Card>

                    {
                        this.state.user && this.state.user.email &&
                        this.state.user.role == USER_ROLE_ORGANIZER &&
                        <View>
                            <Prompt
                                visible={this.state.createSectionDialogVisible}
                                message="Создание новой секции"
                                onPositive={this._onSectionDefined}
                                onCancel={this._cancelSectionCreation} />
                        </View>
                    }
                </View>
            </View >
        );
    }

    _createNewSection = () => {
        this.setState({ createSectionDialogVisible: true });
    }
    _cancelSectionCreation = () => {
        this.setState({ createSectionDialogVisible: false });
    }
    _onSectionDefined = (sectionName) => {
        this.setState({ createSectionDialogVisible: false });
        this.props.createSection(this.state.fullEvent, sectionName);
    }

    _convertToSectionList = (ev) => {
        var z = select(ev.sections).map((val, index, arr) => {
            return { data: select(val.reports), key: index, meta: { id: index, name: val.name } };
        });
        return z;
    }

    _renderSection = ({ section }) => (
        <TouchableHighlight
            onPress={() => this._onHeaderPress(section)}>
            <View
                key={section.meta.id}
                style={styles.listHeaderContainer}>
                <Text style={styles.listHeader}> {section.meta.name} </Text>
                <Text style={styles.listHeaderCounter}> {section.data.length} </Text>
            </View>
        </TouchableHighlight>
    )

    _renderReport = (report, index, section) => {
        if (report.item.sectionTitle != this.state.selectedSectionId) {
            return null;
        }

        return (
            <View>
                <TouchableHighlight
                    onPress={() => this._onReportPress(report)}>
                    <Text style={styles.listItem} key={report.item.reportId}> {report.item.name} </Text>
                </TouchableHighlight>
            </View>
        );
    }

    _onHeaderPress = (section) => {
        this.setState({
            selectedSectionIndex: section.meta.id,
            selectedSectionId: section.meta.name,
            refresh: !this.state.refresh,       // importsnt for list re-render
        });
    }

    _onReportPress = ({ item }) => {
        this.props.reportSelected(item);
        this.props.navigation.navigate('Report', {
            id: item.reportId,
            report: item,
            eventId: this.state.id,
            title: item.name,
            user: this.state.user,
        });
    }

    _onNewReport = () => {
        this.props.navigation.navigate('NewReport', {
            eventId: this.state.fullEvent.id,
            sections: this.state.fullEvent.sections,
            selectedSectionIndex: this.state.selectedSectionIndex,
        });
    }
    _edit = () => {
        this.props.navigation.navigate('NewEvent', {
            event: this.state.fullEvent,
            editing: true,
        });
    }

    _cancelEvent = () => {
        this.props.cancelEvent(this.state.fullEvent.id, this._onClosed, this._onStatusChangeFailure);
    }
    _restoreEvent = () => {
        this.props.restoreEvent(this.state.fullEvent.id, this._onClosed, this._onStatusChangeFailure);
    }
    _deleteEvent = () => {
        this.props.deleteEvent(this.state.fullEvent.id, this._onClosed, this._onStatusChangeFailure);
    }
    _onClosed = () => {
        this.props.navigation.goBack();
    }
    _onStatusChangeFailure = (err) => {
        Alert.alert("Ошибка выполнения операции", JSON.stringify(err));
    }
}

export class EventDetailsScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`,
        headerRight: (navigation.state.params.user && navigation.state.params.user && navigation.state.params.user.role != USER_ROLE_LISTENER)
            ? (
                <View>
                    <Menu>
                        <MenuTrigger>
                            <Icon
                                style={styles.optionsMenu}
                                type={"Ionicons"}
                                name={"more-vert"}
                                size={40} />
                        </MenuTrigger>

                        <MenuOptions>
                            {
                                navigation.state.params.user && navigation.state.params.user &&
                                navigation.state.params.user.role == USER_ROLE_ORGANIZER &&
                                navigation.state.params.user.email == navigation.state.params.event.organizer.email &&

                                <View>
                                    <MenuOption
                                        onSelect={() => {
                                            if (!self) return;
                                            self._edit();
                                        }}>
                                        <Text style={styles.optionsMenuItem}>Редактировать</Text>
                                    </MenuOption>

                                    <MenuOption
                                        onSelect={() => {
                                            if (!self) return;
                                            self._createNewSection();
                                        }}>
                                        <Text style={styles.optionsMenuItem}>Добавить секцию</Text>
                                    </MenuOption>

                                    <MenuOption
                                        onSelect={() => {
                                            if (!self) return;
                                            if (navigation.state.params.isPlanned) {
                                                self._cancelEvent();
                                            } else {
                                                self._restoreEvent();
                                            }
                                        }}>
                                        <Text style={styles.optionsMenuItem}>{navigation.state.params.isPlanned ? "Отменить событие" : "Вернуть событие"}</Text>
                                    </MenuOption>

                                    <MenuOption
                                        onSelect={() => {
                                            if (!self) return;
                                            self._deleteEvent();
                                        }}>
                                        <Text style={styles.optionsMenuItem}>Удалить событие</Text>
                                    </MenuOption>
                                </View>
                            }

                            {
                                navigation.state.params.user && navigation.state.params.user &&
                                (navigation.state.params.user.role == USER_ROLE_ORGANIZER ||
                                    navigation.state.params.user.role == USER_ROLE_REPORTER) &&

                                <MenuOption
                                    onSelect={() => {
                                        if (!self) return;
                                        self._onNewReport();
                                    }}>
                                    <Text style={styles.optionsMenuItem}>Добавить доклад</Text>
                                </MenuOption>
                            }
                        </MenuOptions>
                    </Menu>
                </View>
            )
            : null,
    });

    constructor(props) {
        super(props);
        this.state = {
            self: self,
        }
    }

    render() {
        return (
            <EventDetailsComponent
                event={this.props.event}
                navigation={this.props.navigation}
                createSection={this.props.createSection}
                cancelEvent={this.props.cancelEvent}
                restoreEvent={this.props.restoreEvent}
                deleteEvent={this.props.deleteEvent}
                reportSelected={this.props.reportSelected} />
        );
    }
}

const mapStateToProps = (state) => ({
    event: state.events.currentEvent,
});
const mapDispatchToProps = {
    createSection: createSection,
    cancelEvent: cancelEvent,
    restoreEvent: restoreEvent,
    deleteEvent: deleteEvent,
    reportSelected: reportSelected,
};

const EventDetailConnect = connect(mapStateToProps, mapDispatchToProps)(EventDetailsScreen);
export default EventDetailConnect;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-between",
    },
    optionsMenu: {
        marginHorizontal: 15,
    },
    optionsMenuItem: {
        height: 40,
        fontSize: 20,
    },

    headerContainer: {
        flexDirection: 'row',
        margin: 15,
    },
    eventDescription: {
        fontSize: 18,
        fontWeight: "600",
        margin: 8,
    },
    eventTagsContainer: {
        flexDirection: "row",
    },
    tag: {
        marginHorizontal: 4,
    },
    eventStatusContainer: {
        flexDirection: "row",
        margin: 12,
    },
    eventStatus: {
        width: "50%",
    },
    eventDate: {
        width: "50%",
        alignSelf: "flex-end",
    },
    separator: {
        height: 1,
        width: "100%",
        backgroundColor: "#CED0CE",
    },

    list: {
        margin: 15,
    },
    listHeaderContainer: {
        flexDirection: "row",
        marginVertical: 3,
        borderWidth: 2,
        borderStyle: "solid",
        borderColor: "black",
        borderRadius: 3,
    },
    listHeader: {
        fontSize: 20,
        fontWeight: "bold",
        fontStyle: "italic",
        width: "90%",
    },
    listHeaderCounter: {
        fontSize: 20,
        backgroundColor: "white",
        textAlign: "center",
        width: "10%"
    },
    listItem: {
        fontSize: 17,
        paddingLeft: 20,
        minHeight: 30,
        marginVertical: 3,
    },
    createReport: {
        width: "80%",
        alignSelf: "center",
        backgroundColor: "green",
        color: "white",
    },

    centeredContainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    centeredLabel: {
        fontSize: 32,
    },
});