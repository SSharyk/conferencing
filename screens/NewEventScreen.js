import React, { Component } from 'react';
import { Text, TextInput, Button, View, StyleSheet } from 'react-native'
import DatePicker from 'react-native-datepicker'

import { connect } from 'react-redux';
import EventModel from '../models/event';
import { FormGroup } from '../components/FormGroup';
import AsyncStorageHelper from '../utilities/AsyncStorageHelper';
import { getTomorrow } from '../utilities/calendar';
import { createNewEvent, update } from '../redux/actions/actionsEvents';
import { Card } from 'react-native-elements';

class NewEventComponent extends Component {
    constructor(props) {
        super(props);

        let editing = props.navigation.getParam('editing', false);
        if (editing) {
            let event = props.navigation.getParam('event', null);
            if (event) {
                this.state = {
                    isEditing: true,
                    id: event.id,
                    name: event.name, nameValidationError: null,
                    description: event.description, descriptionValidationError: null,
                    date: new Date(event.date),
                    tags: event.tags,
                }
            }
            return;
        } else {
            this.state = {
                isEditing: false,
                name: '', nameValidationError: null,
                description: '', descriptionValidationError: null,
                date: getTomorrow(),
                tags: '',
            }
        }
    }

    render() {
        if (this.props.error) {
            return (
                <View style={styles.container}>
                    <Text> {JSON.stringify(this.props.error)} </Text>
                </View>
            )
        }

        return (
            <View
                style={styles.container}>

                <FormGroup
                    label={"Название"}
                    minLength={4}
                    initVal={this.state.name}
                    onChange={this.handleNameChange} />

                <FormGroup
                    label={"Описание"}
                    minLength={20}
                    lines={3}
                    initVal={this.state.description}
                    onChange={this.handleDescriptionChange} />

                <DatePicker
                    style={[styles.formItem]}
                    mode="date"
                    placeholder="Дата проведения"
                    format="YYYY-MM-DD"
                    minDate={getTomorrow()}
                    confirmBtnText="Принять"
                    cancelBtnText="Отмена"
                    date={this.state.date}
                    onDateChange={this.handleDateChange} />

                <FormGroup
                    label={"Ключевые слова"}
                    initVal={this.state.tags}
                    onChange={this.handleTagsChange} />

                <Card>
                    <Button
                        style={[styles.formItem, styles.button]}
                        onPress={this.handleButtonPress}
                        title={"Создать"}>
                    </Button>
                </Card>

            </View>
        );
    }

    handleNameChange = (name) => {
        this.setState({ name: name })
    }
    handleDescriptionChange = (description) => {
        this.setState({ description: description })
    }
    handleTagsChange = (tags) => {
        this.setState({ tags: tags })
    }
    handleDateChange = (date) => {
        this.setState({ date: date })
    }

    handleButtonPress = async () => {
        let user = await AsyncStorageHelper.get(AsyncStorageHelper.KEY_USER);
        let event = EventModel.createNew(
            this.state.name, this.state.description,
            this.state.date, this.state.tags,
            user);

        if (this._isModelValid(event)) {
            if (this.state.isEditing) {
                event.id = this.state.id;
                this.props.update(event, this._onCreated, this._onFailure)
            } else {
                this.props.createNew(event, this._onCreated, this._onFailure);
            }
        }
    }

    _isModelValid = (event) => {
        let validFlag = true;
        if (!event.name || event.name.length == 0) {
            validFlag = false;
            this.setState({ nameValidationError: "Название должно быть заполнено" });
        } else {
            this.setState({ nameValidationError: null });
        }

        if (!event.description || event.description.length == 0) {
            validFlag = false;
            this.setState({ descriptionValidationError: "Описание должно быть заполнено" });
        } else {
            this.setState({ descriptionValidationError: null });
        }

        return validFlag;
    }

    _onCreated = (item) => {
        this.props.navigation.navigate('Event');
    }

    _onFailure = (err) => {
        alert("FAILURE: ", JSON.stringify(err));
    }
}

export class NewEventScreen extends Component {

    static navigationOptions = {
        title: "Создание нового события",
    };

    render() {
        return (
            <NewEventComponent
                navigation={this.props.navigation}
                createNew={this.props.createNew}
                update={this.props.update} />
        )
    }
}

const mapStateToProps = (state) => ({
    error: state.events.createEventError,

});
const mapDispatchToProps = {
    createNew: createNewEvent,
    update: update,
};

const NewEventConnect = connect(mapStateToProps, mapDispatchToProps)(NewEventScreen);
export default NewEventConnect;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    formItem: {
        height: 40,
        width: "90%",
        margin: 10,
        borderRadius: 5,
        padding: 3,
    },
    textInput: {
        backgroundColor: '#ffffff',
        flexWrap: "wrap",
    },
    textArea: {
        height: 120,
    },
    list: {
        backgroundColor: 'white',
    },
    button: {
        backgroundColor: '#88cc88',

        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
    },
    validation: {
        color: "red",
    },
});