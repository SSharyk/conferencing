import React, { Component } from 'react';
import { ActivityIndicator, ALert, FlatList, Text, Image, View, TouchableHighlight, StyleSheet, BackHandler } from 'react-native'
import FAB from 'react-native-fab';
import { SearchBar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import * as firebase from 'firebase';
import { connect } from 'react-redux';

import firebaseService from '../firebase/firebaseService';
import select from '../firebase/dataSelector';
import { loadEventsList, eventSelected } from '../redux/actions/actionsEvents';

import { DOMAIN } from '../services/http.service';
import { formatDate } from "../utilities/formatter";

import AsyncStorageHelper from '../utilities/AsyncStorageHelper';
import { USER_ROLE_ORGANIZER } from '../models/user';
import { restoreSession } from '../redux/actions/actionsSession';
import { ProfileScreen } from './ProfileScreen';
import EventsRegistrator from '../utilities/EventsRegistrator';
import { EventRow } from '../components/EventRow';
import { EVENT_STATUSES } from '../models/event';

var self;
class EventsListComponent extends Component {

    constructor(props) {
        super(props);
        self = this;
        this.state = {
            statusIdToFilter: 1,
            search: '',
            allEvents: props.data,
            filteredEvents: props.data,
        }
    }

    componentWillReceiveProps(nextProps, nextCtx) {
        this.setState({ allEvents: nextProps.data, filteredEvents: nextProps.data })
    }

    render() {
        if (this.props.refreshing) {
            return (
                <View style={[styles.centeredContainer, styles.loadingContainer]}>
                    <ActivityIndicator style={styles.loadingIndicator} />
                </View>
            );
        }

        let data = this.state.filteredEvents.filter(this._filter);
        return (
            <View style={styles.eventsList}>
                <SearchBar
                    lightTheme
                    onChangeText={this._changeTextFilter}
                    placeholder="Поиск событий..." />

                <FlatList
                    data={data}
                    renderItem={this._singleEventRender}
                    keyExtractor={this._singleEventKeyExtractor}
                    ItemSeparatorComponent={this._singleItemRenderSeparator} />
                {
                    this.props.user && this.props.user.email &&
                    this.props.user.role == USER_ROLE_ORGANIZER &&
                    <FAB
                        style={styles.fab}
                        buttonColor="blue"
                        onClickAction={this._toNewEvent} />
                }
            </View>
        );
    }

    _changeTextFilter = (val) => {
        val = val.toLowerCase();
        this.setState({
            filteredEvents: this.state.allEvents.filter((item, index, arr) => {
                return item.name.toLowerCase().indexOf(val) != -1 || item.description.toLowerCase().indexOf(val) != -1;
            }),
        });
    }
    _changeStatusFilter = (val) => this.setState({ statusIdToFilter: +val });
    _filter = (item, index, arr) => item.eventStatusId == this.state.statusIdToFilter;
    _textSearchFilter = (item, index, arr) => {
        return item.name.toLowerCase().indexOf(s) != -1 || item.description.toLowerCase().indexOf(s) != -1;
    }

    _singleEventRender = ({ item }) => (
        <EventRow
            event={item}
            onPress={() => this._onSingleEventPress(item)} />
    );

    _singleItemRenderSeparator = () => (
        <View style={styles.separator} />
    );

    _singleEventKeyExtractor = (item, index) => "" + item.id;

    _onSingleEventPress = (item) => {
        this.props.eventSelected(item);
        this.props.navigation.navigate('EventDetails', {
            event: item,
            id: item.id,
            title: item.name,
            user: this.props.user,
            isPlanned: item.eventStatusId == EVENT_STATUSES.Запланировано,
        });
    }

    _toNewEvent = () => {
        this.props.navigation.navigate("NewEvent");
    }
}


export class ScheduleScreen extends Component {

    static navigationOptions = {
        title: 'Расписание событий',
        headerRight: (
            <View>
                <Menu>
                    <MenuTrigger>
                        <Icon
                            style={styles && styles.optionsMenu}
                            type={"MaterialIcons"}
                            name={"filter-list"}
                            size={40} />
                    </MenuTrigger>

                    <MenuOptions>
                        <MenuOption
                            onSelect={() => {
                                if (!self) return;
                                self._changeStatusFilter(1);
                            }}>
                            <Text style={styles && styles.optionsMenuItem}>{"Запланировано"}</Text>
                        </MenuOption>

                        <MenuOption
                            onSelect={() => {
                                if (!self) return;
                                self._changeStatusFilter(4);
                            }}>
                            <Text style={styles && styles.optionsMenuItem}>{"Завершено"}</Text>
                        </MenuOption>
                    </MenuOptions>
                </Menu>
            </View>
        )
    };

    constructor(props) {
        super(props);
        this.state = {
            events: [],
            refreshing: true,
            error: null,
            user: {},
        };

        if (typeof global.self === "undefined") {
            global.self = global;
        }
    }


    async componentWillMount() {
        let self = this;
        EventsRegistrator.addEventListener("sessionChanged", function (user) {
            self.setState({ user: user });
        });
    }

    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        this.props.restore();
        this.props.loadEvents();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        //EventsRegistrator.removeEventListener("sessionChanged", this.onUserSessionChanged);
    }

    handleBackButton = () => {
        BackHandler.exitApp();
    }

    render() {
        const data = select(this.props.events);
        if (this.props.error) {
            Alert.alert("Произошла ошибка", JSON.stringify(this.props.error));
            return null;
        }

        return (
            <View>
                <EventsListComponent
                    navigation={this.props.navigation}
                    user={this.state.user}
                    eventSelected={this.props.eventSelected}
                    data={data} />
            </View>
        )
    }
}

const mapStateToProps = state => ({
    user: state.session.user,
    events: state.events.events,
    error: state.events.loadEventsError,
    refreshing: state.events.refreshing,
});

const mapDispatchToProps = {
    restore: restoreSession,
    loadEvents: loadEventsList,
    eventSelected: eventSelected,
}

const ScheduleConnect = connect(mapStateToProps, mapDispatchToProps)(ScheduleScreen);
export default ScheduleConnect;

const styles = StyleSheet.create({
    container: {
        height: "100%",
        padding: 15,
    },
    optionsMenu: {
        marginHorizontal: 15,
    },
    optionsMenuItem: {
        height: 40,
        fontSize: 20,
    },

    eventsList: {
        width: "90%",
        marginHorizontal: "5%",
        height: "99%"
    },

    separator: {
        height: 1,
        width: "100%",
        backgroundColor: "#CED0CE",
    },
    centeredContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        flexDirection: 'column',
    },
    centeredLabel: {
        fontSize: 32,
    },
    loadingContainer: {
        display: 'flex',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        backgroundColor: '#000000',
        opacity: 0.5,
    },
    loadingIndicator: {
        flex: 1,
        alignItems: 'center',
    },
    filterContainer: {
        position: "absolute",
        bottom: 0,
    },
});