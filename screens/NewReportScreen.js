import React, { Component } from 'react';
import { Alert, Text, TextInput, Button, Picker, View, StyleSheet } from 'react-native'

import { connect } from 'react-redux';
import ReportModel from '../models/report';
import AsyncStorageHelper from '../utilities/AsyncStorageHelper';
import { FormGroup } from '../components/FormGroup';
import { createNewReport } from '../redux/actions/actionsReport';

class NewReportComponent extends Component {
    constructor(props) {
        super(props);

        let selectedSectionIndex = props.navigation.getParam('selectedSectionIndex', 0);
        let sections = props.navigation.getParam('sections', []);
        let eventId = props.navigation.getParam('eventId', null);
        this.state = {
            eventId: eventId,
            sections: sections,
            sectionName: sections[selectedSectionIndex],

            name: '',
            description: '',
            errors: [],
        }
    }

    render() {
        if (this.props.error) {
            return (
                <View style={styles.container}>
                    <Text> {JSON.stringify(this.props.error)} </Text>
                </View>
            )
        }

        return (
            <View
                style={styles.container}>

                <Picker
                    selectedValue={this.state.sectionName}
                    onValueChange={this.handleSectionChange}
                    style={[styles.formItem, styles.list]}
                    mode="dropdown"
                    prompt={"Выберите cекцию"}>
                    {
                        this.state.sections.map((s, sInd) => (
                            <Picker.Item label={s.name} value={s} key={sInd} />
                        ))
                    }
                </Picker>

                <FormGroup
                    label={"Название"}
                    minLength={4}
                    onChange={this.handleNameChange}
                    onValidationError={this.handleValidationErrors}
                    onModelValid={this.handleValidationFixed} />

                <FormGroup
                    label={"Описание"}
                    minLength={20}
                    lines={3}
                    onChange={this.handleDescriptionChange}
                    onValidationError={this.handleValidationErrors}
                    onModelValid={this.handleValidationFixed} />

                <Button
                    style={[styles.formItem, styles.button]}
                    onPress={this.handleButtonPress}
                    title={"Создать"}>
                </Button>

            </View>
        );
    }

    handleNameChange = (name) => {
        this.setState({ name: name })
    }
    handleDescriptionChange = (description) => {
        this.setState({ description: description })
    }
    handleSectionChange = (section) => {
        this.setState({ sectionName: section });
    }

    handleValidationFixed = (fixedErrors) => {
        let existing = this.state.errors;
        for (var index = 0; index < fixedErrors.length; index++) {
            let errIndex = existing.indexOf(fixedErrors[index]);
            if (errIndex != -1) {
                existing.splice(errIndex, 1);
            }
        }
        //let notFixed = existing.filter((item) => fixedErrors.indexOf(item) != -1)
        this.setState({ errors: existing });
    }
    handleValidationErrors = (errors) => {
        let existing = this.state.errors;
        for (var index = 0; index < errors.length; index++) {
            let existingIndex = existing.indexOf(errors[index]);
            if (existingIndex == -1) {
                existing.push(errors[index]);
            }
        }
        //existing.concat(errors);
        this.setState({ errors: existing });
    }

    handleButtonPress = async () => {
        let user = await AsyncStorageHelper.get(AsyncStorageHelper.KEY_USER);
        let files = [];
        let report = ReportModel.createNew(
            this.state.eventId, this.state.sectionName.name,
            this.state.name, this.state.description, user, files);

        if (this._isModelValid()) {
            this.props.createNew(report, this._onCreated, this._onFailure);
        } else {
            Alert.alert("Предупреждение", "В форме присутствуют ошибки");
        }
    }

    _isModelValid = () => {
        return this.state.errors.length == 0;
    }

    _onCreated = (report) => {
        this.props.navigation.goBack();
    }

    _onFailure = (err) => {
        alert("FAILURE: ", JSON.stringify(err));
    }
}

export class NewReportScreen extends Component {

    static navigationOptions = {
        title: "Добавление доклада",
    };

    render() {
        return (
            <NewReportComponent
                navigation={this.props.navigation}
                createNew={this.props.createNew} />
        )
    }
}

const mapStateToProps = (state) => ({
    error: state.reports.createReportError,

});
const mapDispatchToProps = {
    createNew: createNewReport,
};

const NewReportConnect = connect(mapStateToProps, mapDispatchToProps)(NewReportScreen);
export default NewReportConnect;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 25,
    },
    formItem: {
        height: 40,
        width: "90%",
        margin: 10,
        borderRadius: 5,
        padding: 3,
    },
    button: {
        backgroundColor: '#88cc88',

        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
    },
});