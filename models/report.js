class ReportModel {
    constructor(id, eventId, sectionTitle, name, description, organizer, files, messages) {
        this.id = id;
        this.eventId = eventId;
        this.sectionTitle = sectionTitle;
        this.name = name;
        this.description = description;
        this.organizer = organizer;
    }

    static createNew(eventId, sectionTitle, name, description, organizer, files) {
        let timestamp = (new Date()).getTime();
        return new ReportModel(timestamp, eventId, sectionTitle, name, description, organizer, files, []);
    }
}

export default ReportModel;