class MessageModel {
    constructor(email, username, reportId, text, postDate) {
        this.email = email;
        this.username = username;
        this.reportId = reportId;
        this.text = text;
        this.postDate = postDate;
    }

    static createNew(user, reportId, text) {
        let timestamp = (new Date()).getTime();
        return new MessageModel(user.email, user.username, reportId, text, timestamp);
    }
}

export default MessageModel;