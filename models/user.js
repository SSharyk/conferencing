export const USER_ROLE_LISTENER = "Слушатель";
export const USER_ROLE_ORGANIZER = "Организатор";
export const USER_ROLE_REPORTER = "Докладчик";

class UserModel {
    constructor(email, username, role) {
        this.email = email;
        this.username = username;
        this.role = role;
    }
}

export default UserModel;