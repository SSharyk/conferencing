export const EVENT_STATUSES = {
    Запланировано: 1,
    Скрыто: 2,
    Удалено: 3,
    Заверешено: 4,
};
export const EventStatusTitles = ["Запланировано", "Скрыто", "Удалено", "Заверешено"];

class EventModel {
    constructor(id, name, description, date, tags, organizer, status, subscribers) {
        this.id = id,
        this.name = name;
        this.description = description;
        this.date = date;
        this.tags = tags;
        this.organizer = organizer;
        this.eventStatusId = status;
        this.eventStatus = {
            status: EventStatusTitles[status - 1],
            eventStatusId: status,
        };
        this.subscribers = subscribers;
    }

    static createNew(name, description, date, tags, organizer) {
        let timestamp = (new Date()).getTime();
        let creationDate = (new Date(date)).getTime();
        return new EventModel(timestamp, name, description, creationDate, tags, organizer, EVENT_STATUSES.Запланировано, [organizer.email]);
    }
}

export default EventModel;