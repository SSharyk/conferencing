import {Alert} from 'react-native';

class EventsRegistrator {
    static listeners = [];

    static addEventListener(name, fn) {
        let index = EventsRegistrator._getIndexOfListener(name, fn);
        if (index != -1) {
            return;
        }
        EventsRegistrator.listeners.push({
            type: name,
            run: fn,
        });
    }

    static removeEventListener(name, fn) {
        let index = EventsRegistrator._getIndexOfListener(name, fn);
        EventsRegistrator.listeners.splice(index, 1);
    }

    static _getIndexOfListener(eventName, fn) {
        for (let i = 0; i < EventsRegistrator.listeners; i++) {
            if (EventsRegistrator.listeners[i].type == eventName &&
                EventsRegistrator.listeners[i].run == fn) {
                return i;
            }
        }
        return -1;
    }

    static fire(eventName, data) {
        for (let i = 0; i < EventsRegistrator.listeners.length; i++) {
            if (EventsRegistrator.listeners[i].type == eventName) {
                EventsRegistrator.listeners[i].run(data);
            }
        }
    }
}

export default EventsRegistrator;