import { AsyncStorage } from 'react-native';

class AsyncStorageHelper {
    static NAMESPACE = '@Conferencing:';

    static KEY_USER = 'user';

    static async set(key, value) {
        try {
            await AsyncStorage.setItem(`${AsyncStorageHelper.NAMESPACE}${key}`, JSON.stringify(value));
            console.log("set VALUE: " + key + " --> " + value);
        } catch (error) {
            this.logError(error);
        }
    }

    static async get(key) {
        try {
            console.debug("Try get : " + `${AsyncStorageHelper.NAMESPACE}${key}`);
            const value = await AsyncStorage.getItem(`${AsyncStorageHelper.NAMESPACE}${key}`);
            if (value !== null) {
                console.log(`get VALUE: ${key} ----> ${value}`);
                value = JSON.parse(value);
                return value;
            }
        } catch (error) {
            this.logError(error);
        }
        return undefined;
    }

    static async delete(key) {
        try {
            await AsyncStorage.removeItem(`${AsyncStorageHelper.NAMESPACE}${key}`);
            console.debug("Success deleting");
        } catch (error) {
            console.debug("ERROR while delete");
            this.logError(error);
        }
    }

    static async logError(error) {
        console.debug("ERROR while AS: ");
        console.debug(error);
        try {
            await AsyncStorage.setItem(`${AsyncStorageHelper.NAMESPACE}lastError`, JSON.stringify(error));
        } catch (err) {
        }
    }
}

export default AsyncStorageHelper;