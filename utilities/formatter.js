export function formatDate(d, formatString) {
    var date = new Date(d);

    var day = date.getUTCDate();
    var dayS = (day >= 10) ? day : ("0" + day);

    var month = date.getMonth() + 1;
    if (month == 12) {
        month = 1;
    }
    var monthS = (month >= 10) ? month : ("0" + month);

    var year = date.getFullYear();

    return formatString.replace("DD", dayS).replace("MM", monthS).replace("YYYY", year);
}

export function formatTime(d, formatString) {
    let date = (typeof d == "string") ? Date.parse(d) : new Date(d);
    date = new Date(date);

    let hours = toTwoDigits(date.getHours());
    let minutes = toTwoDigits(date.getMinutes());
    let seconds = toTwoDigits(date.getSeconds());

    return formatString.replace("HH", hours).replace("mm", minutes).replace("SS", seconds);
}

function toTwoDigits(val) {
    return (val < 10) ? ("0" + val) : val;
}