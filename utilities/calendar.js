import { formatDate, formatTime } from './formatter';

export function humanizeDuration(date) {
    return formatDate(date, "DD.MM.YYYY") + " " + formatTime(date, "HH:mm");
}

export function getTomorrow() {
    return new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
}