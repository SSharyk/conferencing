import React, { Component } from 'react'
import { ActivityIndicator, StyleSheet } from 'react-native'
import { PropTypes } from 'prop-types'

import { connect } from 'react-redux'

import { restoreSession } from '../redux/actions/actionsSession'
import MainPageTabNavigator from '../core/rootNavigator'

const ConferencingAppComponent = props => {
    return <MainPageTabNavigator />
}

class ConferencingAppContainer extends Component {

    componentDidMount() {
        this.props.restore()
    }

    render() {
        return (
            <ConferencingAppComponent
                restoring={this.props.restoring}
                logged={this.props.logged} />)
    }
}

const mapStateToProps = state => ({
    restoring: state.session.restoring,
    logged: state.session.user != null,
})

const mapDispatchToProps = {
    restore: restoreSession
}

ConferencingAppContainer.propTypes = {
    restoring: PropTypes.bool.isRequired,
    logged: PropTypes.bool.isRequired,
    restore: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ConferencingAppContainer)
