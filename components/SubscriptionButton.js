import React, { Component } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { subscribe, unsubscribe } from '../redux/actions/actionsEvents';
import select from '../firebase/dataSelector';

export class SubscriptionButton extends Component {
    constructor(props) {
        super(props);

        let index = select(props.event.subscribers).indexOf(props.userEmail);

        this.state = {
            isSubscribed: index != -1,
        }
    }

    render() {
        if (this.props.event.organizer.email == this.props.userEmail) {
            return (
                <View>
                    <Button
                        title="Вы организатор этого события"
                        onPress={() => { }}
                        style={styles.button} />
                </View>
            );
        }
        if (this.state.isSubscribed) {
            return (
                <View>
                    <Button
                        style={styles.button}
                        title="Отказаться"
                        onPress={this._unsubscribe} />
                </View>
            );
        } else {
            return (
                <View>
                    <Button
                        style={styles.button}
                        title="Подписаться"
                        onPress={this._subscribe} />
                </View>
            );
        }
    }

    _subscribe = () => {
        this.props.subscribe(this.props.event, this.props.userEmail);
        this.setState({ isSubscribed: true });
    }

    _unsubscribe = () => {
        this.props.unsubscribe(this.props.event, this.props.userEmail);
        this.setState({ isSubscribed: false });
    }
}

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = {
    subscribe: subscribe,
    unsubscribe: unsubscribe,
};

const SubscriptionButtonConnect = connect(mapStateToProps, mapDispatchToProps)(SubscriptionButton);
export default SubscriptionButtonConnect;

const styles = StyleSheet.create({
    button: {
        width: "90%",
        marginHorizontal: "5%",
        marginBottom: 30,
        backgroundColor: "green",
    },
});