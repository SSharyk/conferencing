import React from 'react';
import { View, ActivityIndicator, TextInput, Button, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Card } from 'react-native-elements';

export class ChatBottom extends React.Component {
    constructor() {
        super();
        this.state = {
            text: "",
        };
    }

    render() {
        return (
            <Card>
                <View style={[styles.chatBottomContainer, this.props.style]}>
                    <TextInput
                        placeholder="Введите сообщение для отправки"
                        editable={true}
                        onChangeText={this._saveDraft}
                        value={this.state.text}
                        style={styles.newMessageInputField} />

                    {
                        this.props.isSending &&
                        <ActivityIndicator size={40} />
                    }
                    {
                        !this.props.isSending &&
                        <Icon
                            name="send"
                            size={40}
                            onPress={this._trySend} />
                    }
                </View>
            </Card>
        );
    }

    _saveDraft = (text) => this.setState({ text: text });

    _trySend = () => {
        if (this.props.isSending)
            return;
        this.props.onSend(this.state.text, this._clear, this._onFailure);
    }

    _clear = () => {
        this.setState({ text: '' });
    }
    _onFailure = (err) => {
        alert("Failure: " + JSON.stringify(err));
    }
}

const styles = StyleSheet.create({
    chatBottomContainer: {
        justifyContent: "flex-end",
        flexDirection: "row",
    },
    newMessageInputField: {
        flex: 1,
        alignItems: "stretch",
    },
    newMessageSendButton: {
        flex: 1,
        justifyContent: "flex-end",
    },
});