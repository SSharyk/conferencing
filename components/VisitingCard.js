import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Card } from 'react-native-elements';
import { Gravatar, GravatarApi } from 'react-native-gravatar';

export class VisitingCard extends React.Component {
    render() {
        let gravatarOptions = {
            email: this.props.email,
            parameters: { "size": GRAVATAR_RADIUS, "d": "wavatar" },
        };
        return (
            <Card title={this.props.title}>
                <View style={styles.authorContainer}>
                    <Gravatar
                        options={gravatarOptions}
                        style={styles.roundedProfileImage} />

                    <View style={styles.infoMainContainer}>
                        <View style={styles.infoContainer}>
                            <Text style={[styles.info, styles.infoLabel]}> {"Email"} </Text>
                            <Text style={[styles.info, styles.infoValue]}> {this.props.email} </Text>
                        </View>
                        <View style={styles.infoContainer}>
                            <Text style={[styles.info, styles.infoLabel]}> {"Логин"} </Text>
                            <Text style={[styles.info, styles.infoValue]}> {this.props.login} </Text>
                        </View>
                    </View>
                </View>
            </Card>
        );
    }
}

const GRAVATAR_RADIUS = 100;
const styles = StyleSheet.create({
    authorContainer: {
        flexDirection: 'row',
    },
    infoMainContainer: {
        flex: 4,
        justifyContent: "center",
    },
    infoContainer: {
        flexDirection: "row",
    },
    info: {
        fontSize: 20,
        flex: 1,
    },
    infoValue: {
        fontWeight: "600",
    },
    infoLabel: {
        fontStyle: "italic",
    },
    roundedProfileImage: {
        width: GRAVATAR_RADIUS,
        height: GRAVATAR_RADIUS,
        marginVertical: 20,
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: GRAVATAR_RADIUS / 2,
    },
});