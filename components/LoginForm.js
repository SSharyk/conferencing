import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, TextInput, Text, Button, Alert, ActivityIndicator, TouchableHighlight, Picker, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import * as firebase from 'firebase';
import firebaseService from '../firebase/firebaseService';

import { FormGroup } from '../components/FormGroup';

export class LoginForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            role: "Организатор",
            isLoginMode: true,
        }
    }

    handleEmailChange = (email) => {
        this.setState({ email: email })
    }

    handlePasswordChange = (password) => {
        this.setState({ password: password })
    }

    handleUsernameChange = (username) => {
        this.setState({ username: username })
    }

    handleRoleChange = (r) => {
        this.setState({ role: r });
    }

    handleButtonPress = () => {
        let email = this.state.email;
        let pass = this.state.password;
        try {
            if (this.state.isLoginMode) {
                this.props.login(email, pass)
            } else {
                let role = this.state.role;
                let username = this.state.username;
                this.props.signup(email, pass, username, role)
            }
        } catch (err) {
            Alert.alert("Произошла ошибка", err.message);
        }
    }

    onSwitchMode = () => {
        this.setState({ isLoginMode: !this.state.isLoginMode });
    }

    componentDidUpdate(prevProps) {
        if (this.props.error && prevProps.error != this.props.error) {
            Alert.alert("Произошла ошибка", this.props.error);
        }
    }


    render() {
        return (
            <View
                style={styles.container}>

                <FormGroup
                    label={"Email"}
                    icon={"email"}
                    minLength={5}
                    keyboardType='email-address'
                    onChange={this.handleEmailChange} />

                <FormGroup
                    label={"Пароль"}
                    icon={"security"}
                    minLength={6}
                    password={true}
                    onChange={this.handlePasswordChange} />

                {
                    !this.state.isLoginMode &&
                    <View>
                        <FormGroup
                    label={"Логин"}
                    icon={"person"}
                    minLength={5}
                    onChange={this.handleUsernameChange} />

                        <Picker
                            selectedValue={(this.state && this.state.role) || 'Cлушатель'}
                            onValueChange={this.handleRoleChange}
                            style={[styles.formItem, styles.list]}
                            mode="dropdown"
                            prompt={"Выберите роль"}>
                            <Picker.Item label="Слушатель" value="Слушатель" />
                            <Picker.Item label="Организатор" value="Организатор" />
                            <Picker.Item label="Докладчик" value="Докладчик" />
                        </Picker>
                    </View>
                }

                <Button
                    style={[styles.formItem, styles.button]}
                    onPress={this.handleButtonPress}
                    title={"Вход"}>
                </Button>

                <TouchableHighlight
                    style={[styles.formItem, styles.switchModeLabel]}
                    onPress={this.onSwitchMode}>
                    <Text> {(this.state.isLoginMode) ? "Нет аккаунта? Создать новый!" : "Уже есть аккаунт? Войти!"} </Text>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    formItem: {
        height: 40,
        margin: 10,
        borderRadius: 5,
        padding: 3,
    },
    textInput: {
        backgroundColor: '#ffffff',
    },
    switchModeLabel: {
        alignItems: 'center',
    },
    list: {
        backgroundColor: 'white',
    },
    button: {
        backgroundColor: '#88cc88',

        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
    },
});