import React from 'react';
import { View, Text, Image, TouchableHighlight, StyleSheet } from 'react-native';

import { formatDate } from "../utilities/formatter";
import { DOMAIN } from '../services/http.service';

export class EventRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const item = this.props.event;

        return (
            <TouchableHighlight
                onPress={this.props.onPress}>
                <View style={styles.itemContainer}>
                    <View style={styles.eventLogo}>
                        <Image source={{ uri: `${DOMAIN}Files/Logo/${item.id}.png` }}
                            resizeMode="contain"
                            style={{ flex: 1 }} />
                    </View>

                    <View style={styles.eventInfo}>
                        <Text style={styles.eventHeader}>{item.name}</Text>
                        <Text style={styles.eventDescription}>{item.description}</Text>
                        <View style={styles.eventStatusContainer}>
                            <Text style={styles.eventStatus}>{item.eventStatus.status}</Text>
                            <Text style={styles.eventDate}>{formatDate(item.date, "DD.MM.YYYY")}</Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        width: "100%",
        marginTop: 10,
        marginBottom: 10,
        flexDirection: "row",
    },
    eventInfo: {
        width: "85%",
    },
    eventHeader: {
        fontWeight: "600",
        fontSize: 20,
        flex: 1,
        flexWrap: "wrap",
    },
    eventDescription: {
        fontSize: 18,
        flex: 1,
        flexWrap: "wrap",
    },
    eventStatusContainer: {
        flexDirection: "row",
    },
    eventStatus: {
        width: "50%",
    },
    eventDate: {
        width: "50%",
        alignSelf: "flex-end",
    },
    eventLogo: {
        width: "14%",
        minWidth: "10%",
        minHeight: "10%",
        margin: 0,
        paddingRight: "3%",
    },
});