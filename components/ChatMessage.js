import PropTypes from 'prop-types';
import React from 'react';
import { View, Image, Text, ViewPropTypes, StyleSheet } from 'react-native';
import { Gravatar, GravatarApi } from 'react-native-gravatar';

import { DOMAIN } from '../services/http.service';
import { humanizeDuration } from '../utilities/calendar';

export class ChatMessage extends React.Component {

    render() {
        let mes = this.props.message.item;
        let isMine = mes.email == this.props.userEmail;
        const gravatarOptions = {
            email: mes.email,
            parameters: { "size": GRAVATAR_RADIUS, "d": "wavatar" },
        };

        return (
            <View style={[styles.container, (isMine ? styles.alignEnd : styles.alignStart)]}>
                {
                    !isMine &&
                    <Gravatar
                        options={gravatarOptions}
                        style={[styles.image, styles.avatar, styles.roundedProfileImage]} />
                }

                <View style={[styles.messageContainer, (isMine ? styles.my : styles.foreign)]}>
                    {
                        !isMine &&
                        <Text style={styles.messageAuthor}> {mes.username} </Text>
                    }
                    <Text style={styles.messageText}> {mes.text} </Text>
                    <Text style={styles.messageDateTime}> {humanizeDuration(mes.postDate)}</Text>
                </View>

                {
                    isMine &&
                    <Gravatar
                        options={gravatarOptions}
                        style={[styles.image, styles.avatar, styles.roundedProfileImage]} />
                }
            </View>
        );
    }
}

const GRAVATAR_RADIUS = 32;
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: "flex-start",
        marginHorizontal: 15,
    },
    alignStart: {
        justifyContent: "flex-start",
    },
    alignEnd: {
        justifyContent: "flex-end",
    },
    messageContainer: {
        margin: 10,
        borderRadius: 5,
        maxWidth: "75%",
        borderColor: "white",
    },
    foreign: {
        backgroundColor: '#add8e6',
        justifyContent: "flex-start",
    },
    my: {
        backgroundColor: '#d3d3d3',
        justifyContent: "flex-end",
    },
    messageAuthor: {
        color: "white",
        fontWeight: "bold",
        fontSize: 15,
    },
    messageText: {
        fontSize: 16,
        color: "black",
    },
    messageDateTime: {
        fontStyle: "italic",
        justifyContent: "flex-end",
    },
    image: {
        width: GRAVATAR_RADIUS,
        height: GRAVATAR_RADIUS,
    },
    avatar: {
        justifyContent: "flex-start",
        marginVertical: 10,
    },
    myAvatar: {
        justifyContent: "flex-end",
        marginVertical: 10,
    },
    roundedProfileImage: {
        width: GRAVATAR_RADIUS,
        height: GRAVATAR_RADIUS,
        marginVertical: 20,
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: GRAVATAR_RADIUS / 2,
    },
});