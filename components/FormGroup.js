import React, { Component } from 'react';
import { View, Text, TextInput, Image, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';

export class FormGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFirstTime: true,
            value: props.initVal || '',
            isValid: true,
            errorsFound: [],
            errorsFixed: [],
            validationErrors: [],
        };

        this.checkValid = this.checkValid.bind(this);
        this._checkValid = this._checkValid.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    render() {
        if (this.state.isFirstTime) {
            this.checkValid();
            this.setState({ isFirstTime: false });
        }
        return (
            <View style={styles.formGroupContainer}>
                <View
                    style={[styles.labelContainer, { flexDirection: this.props.direction }]}>
                    <Text style={[styles.label]}> {this.props.label} </Text>
                    <View
                        style={[styles.inputContainer, { height: this.props.lines * DEFAULT_HEIGHT + 2 * BORDER_WIDTH }]}>
                        {
                            this.props.icon &&
                            <Icon
                                style={[styles.icon]}
                                size={DEFAULT_HEIGHT - 8}
                                name={this.props.icon} />
                        }
                        <TextInput
                            style={[styles.formItem, styles.textInput, { height: this.props.lines * DEFAULT_HEIGHT }]}
                            returnKeyType='next'
                            autoCapitalize='characters'
                            keyboardType={this.props.keyboardType || 'text'}
                            secureTextEntry={this.props.password || false}
                            underlineColorAndroid={'transparent'}
                            placeholder={this.props.label}
                            onChangeText={this.handleTextChange}
                            value={this.state.value} />
                    </View>
                </View>

                {
                    this.state.validationErrors.map((err, index) => (
                        <Text
                            key={index}
                            style={styles.validation}>
                            {err}
                        </Text>
                    ))
                }
            </View>
        );
    }

    handleTextChange = (txt) => {
        this.setState({ value: txt });
        this.checkValid();
        this.props.onChange(txt);
    }

    checkValid() {
        let previousErrors = this.state.validationErrors;
        this.setState({ isValid: true, errorsFixed: [], errorsFound: [] });
        let validFlag = true;
        const lenLabel = `Длина поля '${this.props.label}' должна быть не менее ${this.props.minLength} символов`;
        const patternLabel = `Запись в поле '${this.props.label}' не соответствует формату`;
        if (this.props.minLength) {
            validFlag &= this._checkValid(
                () => { return this.state.value.trim().length < this.props.minLength; },
                lenLabel);
        }
        if (this.props.pattern) {
            validFlag &= this._checkValid(this._patternCheck, patternLabel);
        }
        /// TODO: regex

        this.setState({ isValid: validFlag, validationErrors: this.state.errorsFound });
        if (this.props.onModelValid) {
            this.props.onModelValid(this.state.errorsFixed);
        }
        if (this.props.onValidationError) {
            this.props.onValidationError(this.state.errorsFound);
        }
    }

    _patternCheck = () => {
        var term = this.state.value;
        var re = new RegExp(this.props.pattern);
        return !(re.test(term));
    }

    _checkValid(predicate, errorMessage) {
        if (predicate()) {
            let existing = this.state.errorsFound;
            let index = existing.indexOf(errorMessage);
            if (index == -1) {

                existing.push(errorMessage);
                this.setState({ errorsFound: existing });
            }
            return false;
        } else {
            let existing = this.state.errorsFixed;
            let index = existing.indexOf(errorMessage);
            if (index == -1) {
                existing.push(errorMessage);
                this.setState({ errorsFixed: existing });
            }
            return true;
        }
    }
}

FormGroup.defaultProps = {
    direction: 'column',
    lines: 1,
};

FormGroup.propTypes = {
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,

    onValidationError: PropTypes.func,
    onModelValid: PropTypes.func,
    direction: PropTypes.string,
    icon: PropTypes.string,
    lines: PropTypes.number,
    minLength: PropTypes.number,
    regex: PropTypes.string,
};

const BORDER_WIDTH = 2;
const DEFAULT_HEIGHT = 40;

const styles = StyleSheet.create({
    formGroupContainer: {
        width: "100%",
        margin: 10,
    },
    inputContainer: {
        flexDirection: 'row',
        borderWidth: BORDER_WIDTH,
        borderStyle: "solid",
        borderColor: "black",
        borderRadius: 3,
        margin: 10,
    },
    formItem: {
        // width: "100%",
        borderRadius: 5,
        flex: 1,
        padding: 3,
        marginBottom: 4,
    },
    textInput: {
        backgroundColor: '#ffffff',
        flexWrap: "wrap",
    },
    label: {
        marginLeft: 10,
        fontWeight: "600",
    },
    icon: {
        width: 32,
        height: 32,
        alignSelf: "center",
    },
    validation: {
        color: "red",
        margin: 10,
    },
});