type BackButtonProps = {
    navigation: NavigationScreenProp<NavigationStateRoute>,
};

class MyBackButton extends React.Component<BackButtonProps, any> {
    render() {
        return (
            <HeaderButtons>
                <HeaderButtons.Item title="Back" onPress={this._navigateBack} />
            </HeaderButtons>
        );
    }

    _navigateBack = () => {
        if (this.props.navigation.routeName != "Event") {
            this.props.navigation.goBack(null);
        }
    };
}