/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { Provider } from 'react-redux';
import { MenuProvider } from 'react-native-popup-menu';

import ConferencingAppContainer from './ConferencingApp';
import { configureStore } from '../redux/store';

type MyNavScreenProps = {
    navigation: NavigationScreenProp<NavigationState>,
    banner: React.Node,
};

const store = configureStore();

export default class App extends Component<MyNavScreenProps> {
    constructor(props) {
        super(props);
        if (typeof global.self === "undefined") {
            global.self = global;
        }
    }

    render() {
        return (
            <Provider store={store}>
                <MenuProvider>
                    <ConferencingAppContainer />
                </MenuProvider>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});
