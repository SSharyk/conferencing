import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Dialog from "react-native-dialog";

class Prompt extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
        }

        this.handleCancel = this.handleCancel.bind(this);
        this.handleOk = this.handleOk.bind(this);
    }

    handleValueChange = (txt) => {
        this.setState({ value: txt });
    }
    handleOk = () => {
        if (this.props.onPositive) {
            this.setState({ value: '' });
            this.props.onPositive(this.state.value);
        }
    }
    handleCancel = () => {
        if (this.props.onCancel) {
            this.props.onCancel();
        }
    }

    render() {
        return (
            <Dialog.Container visible={this.props.visible}>
                <Dialog.Title>{this.props.title}</Dialog.Title>
                <Dialog.Description>{this.props.message}</Dialog.Description>
                <Dialog.Input label={"Введите значение"} onChangeText={this.handleValueChange} value={this.state.value} />
                <Dialog.Button label="Отмена" onPress={this.handleCancel} />
                <Dialog.Button label="OK" onPress={this.handleOk} />
            </Dialog.Container>
        );
    }
}

export default Prompt;

const styles = StyleSheet.create({
    modelContainer: {
        justifyContent: "center",
        alignItems: "center",
        height: 300,
        width: "80%",
        marginHorizontal: "10%",
    },
    dialogButtonContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    dialogButton: {
        margin: 10,
    },
});